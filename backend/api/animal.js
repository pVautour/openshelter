import Router from 'koa-router'
import Animal from '../models/animal'

const router = new Router({ prefix: '/animal' })

router.get('/', async (ctx, next) => {
    await Animal.find()
    .then(animals => {
        ctx.body = animals
    })
})

router.get('/:id', async (ctx, next) => {
    await Animal.findById(ctx.params.id)
    .then(animals => {
        ctx.body = animals
    })
})

router.post('/', async (ctx, next) => {
    let animal = new Animal(ctx.request.body)
    await animal.save()
    .then(data => {
        ctx.body = data
    })
    .catch(err => {
        ctx.body = 'error: ' + err
    })
    await next()
})

router.put('/:id', async (ctx, next) => {
    console.log(ctx.request.body.name)
    console.log(ctx.request.body._id)
    let animal = new Animal(ctx.request.body)
    await Animal.findOneAndReplace({_id: animal._id}, animal)
    .then(data => {
        ctx.body = data
    })
    .catch(err => {
        ctx.body = 'error: ' + err
    })
    await next()
})

router.delete('/:id', async (ctx, next) => {
    await Animal.findOneAndDelete(ctx.request.body)
    .then(data => {
        ctx.body = data
    })
    .catch(err => {
        ctx.body = 'error: ' + err
    })
    await next()
})

export default router