import mongoose from 'mongoose'

const Animal = new mongoose.Schema({
    name: {
        type: String,
        lowercase: true,
        required: true,
    },
    dateOfBirth: {
        type: Date
    },
    dateOfDeath: {
        type: Date
    },
});

// Animal.path('name').set(function (n) {
//     return toLowerCase(n);
// });

export default mongoose.model('Animal', Animal)